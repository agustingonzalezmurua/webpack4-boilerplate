import webpack from "webpack"
import path from "path"
import { TsconfigPathsPlugin } from "tsconfig-paths-webpack-plugin"
const HtmlWebpackPlugin = require("html-webpack-plugin")
const DashboardPlugin = require("webpack-dashboard/plugin")

const config: webpack.Configuration = {
  entry: path.join(__dirname, '..', 'src', 'index.tsx'),
  output: {
    filename: '[name].js',
    path: path.join(__dirname, '..', 'dist'),
    publicPath: '/',
  },
  devtool: 'eval-source-map',
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
    }, {
      test: /\.(sa|sc|c)ss$/,
      use: [
        'style-loader',
        'css-loader?modules',
        'sass-loader',
      ]
    }, {
      test: /\.(jpe?g|png|gif|svg|ico|ttf|woff2|woff|eot)$/,
      loader: 'file-loader',
      options: {
        name: '[hash].[name].[ext]',
        outputPath: 'assets/'
      }
    }]
  },
  resolve: {
    plugins: [
      new TsconfigPathsPlugin({
        configFile: path.join(__dirname, '..', 'tsconfig.json')
      })
    ],
    extensions: [".tsx", ".ts", ".js", ".scss"]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        }
      }
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '..', 'src', 'index.template.html'),
      inject: 'body'
    }),
    new webpack.DefinePlugin({
      PRODUCTION: false
    }),
    new webpack.HotModuleReplacementPlugin(),
    new DashboardPlugin(),
  ],
  devServer: {
    port: 9090,
    historyApiFallback: true,
    hot: true
  }
}

module.exports = config;