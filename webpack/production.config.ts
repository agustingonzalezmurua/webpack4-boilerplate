import webpack from "webpack"
import path from "path"
const { TsconfigPathsPlugin } = require("tsconfig-paths-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const ServiceWorkerWebpackPlugin = require("serviceworker-webpack-plugin")
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const CompressionPlugin = require("compression-webpack-plugin")
const ImageminPlugin = require("imagemin-webpack-plugin").default

const config: webpack.Configuration = {
  entry: path.join(__dirname, '..', 'src/index.tsx'),
  output: {
    filename: '[hash].js',
    path: path.join(__dirname, '..', 'dist'),
    publicPath: '/',
  },

  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
    }, {
      test: /\.(sa|sc|c)ss$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        'sass-loader',
        {
          loader: 'postcss-loader',
          options: {
            parser: 'postcss-scss',
            plugins: [
              require("autoprefixer")
            ]
          }
        }
      ]
    }, {
      test: /\.(jpe?g|png|gif|svg|ico|ttf|woff2|woff|eot)$/,
      loader: 'file-loader',
      options: {
        name: '[hash].[ext]',
        outputPath: 'assets/'
      }
    }]
  },

  resolve: {
    plugins: [
      new TsconfigPathsPlugin({
        configFile: path.join(__dirname, '..', 'tsconfig.json')
      })
    ],
    extensions: [".tsx", ".ts", ".js", ".scss"]
  },

  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors'
        }
      }
    }
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, '..', 'src', 'index.template.html'),
      inject: 'body'
    }),
    new webpack.HashedModuleIdsPlugin(),
    new webpack.DefinePlugin({
      PRODUCTION: true
    }),
    new MiniCssExtractPlugin({
      filename: "[chunkhash].css"
    }),
    new ServiceWorkerWebpackPlugin({
      entry: path.join(__dirname, '..', 'src', 'sw.js'),
      publicPath: "/"
    }),
    new OptimizeCssAssetsPlugin({
      cssProcessorOptions: {
        preset: ['default', { discardComments: { removeAll: true } }]
      }
    }),
    new ImageminPlugin(),
    new CompressionPlugin()
  ],

  devServer: {
    compress: true,
    port: 9090,
    historyApiFallback: true,
  }
}

module.exports = config;