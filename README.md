# Webpack 4 tsx boilerplate

Este proyecto ofrece un empaquetado de configuraciones de wepback para Single Page App

Está diseñado para trabajar con Typescript

Incluye:
- Babel como pre-procesador
- Recursos generados traen una versión comprimida con gzip
- Postcss con autoprefixer
- Eslint
- ServiceWorker para cachear los archivos generados
- Estrategias básicas de caché con hashed modules
- Optimización de assets de CSS
- Optimización de imágenes