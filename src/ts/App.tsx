import React from "react"
import { hot } from "react-hot-loader/root"
import Counter from "~ts/components/Counter"

const App = () => (
    <Counter />
)

export default hot(App)