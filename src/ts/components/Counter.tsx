import React, { PureComponent } from "react"
import { clearInterval } from "timers";
const styles = require("~assets/components/counter.scss")
interface State {
    counter: number
}

export default class Counter extends PureComponent<{}, State> {

    public state = {
        counter: 980
    }

    private interval: NodeJS.Timeout | undefined;

    public componentDidMount(): void {
        this.interval = setInterval(this.increment, 1000)
    }

    public componentWillUnmount(): void {
        clearInterval(this.interval!);
    }

    private increment = () => {
        this.setState(({ counter }) => ({
            counter: counter + 1
        }))
    }

    public render() {
        const { counter } = this.state;
        return (
            <section className={styles.counter}>
                <h1>Contador!</h1>
                <h2>{counter.toLocaleString("es-CL")}</h2>
            </section>
        )
    }
}